import express from 'express';
import { createServer } from 'http';
import { Server } from 'socket.io';
import { join } from 'path';

const app = express();
const server = createServer(app);
const io = new Server(server);

app.use(express.static(join('public')));
app.set('views', join('views'));
app.set('view engine', 'pug');
app.get('/', (req, res) => res.render('page'));

io.on('connection', (socket) => {
    socket.emit('message', { message: 'Welcome to the Real Time Web Chat' });
    socket.on('send', (data) => io.emit('message', data));
});

const port = process.env.PORT || 3700;
server.listen(port, () => console.log(`Server listening on http://localhost:${port}`));
