window.addEventListener('load', () => {
    const messages = [];
    const socket = io.connect();
    const field = document.getElementById('field');
    const sendButton = document.getElementById('send');
    const content = document.getElementById('content');
    const name = document.getElementById('name');

    socket.on('message', (data) => {
        if (!data.message) {
            console.log('There is a problem:', data);
            return;
        }

        messages.push(data);
        let html = '';
        for (let i = 0; i < messages.length; i++) {
            html += `<b>${messages[i].username ? messages[i].username : 'Server'}: </b>`;
            html += `${messages[i].message}<br />`;
        }
        content.innerHTML = html;
        content.scrollTop = content.scrollHeight;

    });

    sendButton.addEventListener('click', () => {
        if (name.value === '') {
            alert('Please type your name!');
            return
        }
        const text = field.value;
        socket.emit('send', { message: text, username: name.value });
        field.value = '';
    });

    field.addEventListener('keypress', (e) => {
        if (e.key === 'Enter') {
            sendButton.click();
        }
    });
});
